# Privacy Policy

Last updated: May 13, 2021

Permission.io, Inc. (“Permission”, “we”, “us” or “our”) respects the privacy of our users (“user”, “you”, or “your”). This Policy applies to information we collect when you use our websites, <a href="https://Permission.io">https://Permission.io</a> and all subdomains of <a href="https://Permission.io">https://Permission.io</a>, our browser extension, or any other media form, media channel, forums, mobile website, or mobile application related or connected thereto provided or officially sponsored by Permission.io (collectively, the “Services”).

Our belief is that any personal information provided to us by you is just that: personal and private. This Privacy Policy (“Privacy Policy”, “Policy”), explains how we collect, use, disclose, and safeguard your information when you use our Services, and your rights under applicable law. Please read this Privacy Policy carefully.

<ol>
<li><a href="#heading1">Definitions</a></li>
<li><a href="#heading2">Personal Data We Collect</a></li>
<li><a href="#heading3">How Long Do We Keep Your Data?</a></li>
<li><a href="#heading4">How Do We Use Your Information?</a></li>
<li><a href="#heading5">When do we share your information?</a></li>
<li><a href="#heading6">Third-party Sites</a></li>
<li><a href="#heading7">What are My Rights with Respect To My Personal Data?</a></li>
<li><a href="#heading8">How does Permission protect my information?</a></li>
<li><a href="#heading9">Policy for Minors</a></li>
<li><a href="#heading10">California Privacy Rights</a></li>
<li><a href="#heading11">Supplemental Notice for Nevada Residents</a></li>
<li><a href="#heading12">Notice to All Non-US Residents</a></li>
<li><a href="#heading13">Changes to This Policy</a></li>
<li><a href="#heading14">How Can You Contact Us About This Policy</a></li>
</ol>

<h2><a id="heading1"></a>1. Definitions</h2>
We believe that our policies should be legible and easy to understand for the general public, as well as our customers and business partners. All terms used below have the definitions provided under applicable laws, such as the EU General Data Protection Regulation (GDPR).

<h2><a id="heading2"></a>2.	Personal Data We Collect</h2>
The personal data we collect is described below:
<h3>Your Permission Account</h3>
When you access or register with the Services, you may be required to provide an email address and password. Once you have created an account, in order to set up your wallet, you will need to provide a photo of yourself, as well as a photo of your government-issued identification, and any other information required under applicable law to verify your identity. In order to receive our ASK currency, you may also choose to provide additional information to us, including but not limited to:

<ul>
<li>your first and last name,</li>
<li>your postal address,</li>
<li>email address, phone number and other similar contact data,</li> 
<li>your age, </li>
<li>your gender, </li>
<li>your nationality,</li>
<li>your occupation,</li>
<li>your annual household income,</li>
<li>your marital status,</li>
<li>your number of children,</li>
<li>your hometown, place of employment and interests.</li>
</ul>

You are free to change or completely remove any information shared with us at any time. However, refusing to provide requested personal data might prevent you from using certain features of the Services. If you use our Services to withdraw and/or transfer your ASK currency to another ASK-compatible wallet, we or our service providers may collect additional information to verify your identity in accordance with our legal obligations.

We may provide you with the option to register for Permission using your existing social media account, like Facebook, LinkedIn, Twitter or other social media providers. Where you choose to do this, we will receive certain profile information about you from your social media provider. The profile information we receive may vary depending on the social media provider concerned, but will often include your name, email address, friends list, profile picture as well as other information you choose to share with us. We will use the information we receive only for the purposes that are described in this privacy policy or that are otherwise made clear to you via the Services. Please note that we do not control, and are not responsible for, other uses of your personal information by your third-party social media provider. We recommend that you review each social media provider’s privacy policy to understand how they collect, use and share your personal information, and how you can set your privacy preferences on their sites and apps.

If you consent to our collection of biometric information, you agree that we may collect your facial geometry from your photograph in order to verify that it matches your government-issued ID. Your biometric information is not shared with any third party and is deleted after the verification is complete.

<h3>Financial data</h3>
When you purchase, order, return, exchange or request information about our services from the Services, you may be asked to share financial data with us related to your payment method. This information may include your valid credit card number, card brand, expiration date, as well as other details necessary to process your payment information. This financial information is processed and stored by our payment processors, such as Stripe and PayPal. We encourage you to review their privacy policies and contact them directly for responses to your questions.

<h3>Our browser extension</h3>
If you use our browser extension, we will collect time-stamped information about what websites you visit (including the URLs, page titles, and metadata descriptions of each page). This information may also include search history and queries, as contained within site URLs. We use the data we have collected about you associated with your Permission Account to show you personalized offers and advertisements within the browser extension. We use the information above to measure your interactions with these ads (for example, whether you click on the ad to visit the marketer’s website) in order to grant you ASK currency.

<h3>Your communications with us</h3>
You may choose to contact us by email or through our Services for a variety of purposes such as product or company inquiries, customer support inquiries and sales requests. When you do this, we collect any information you voluntarily submit to us, such as your first and last name, email address, phone number, job title and company name. We may also provide the opportunity to register for events or conferences, order or request white papers, or participate in online surveys. It is completely up to you to choose whether or not you want to provide it.

<h3>General device data</h3>
Whenever you access or interact with our Services, we collect some general data and information about the request and store the relevant details in server or system log files. This data includes details like your IP address, your browser type and version used, mobile identifiers, MAC address, your device type and operating system, the time and date you accessed the Services, and the pages you viewed directly before and after accessing our Services. Additional detail may be collected or derived from this information for use in the event of an attack on our information technology systems.

To support these efforts, we analyze de-identified or aggregated collected data and information statistically, with the aim of increasing the data protection and security of our company, and to maintain an optimal level of protection for the personal data we process.

<h3>Automated website tracking and cookies</h3>
We partner with selected third-party vendors, described below, which may allow tracking technologies and remarketing services on our Services through the use of cookies, web beacons, tracking pixels, and other tracking technologies (“Technologies”). Our use of Technologies fall into the following categories: 

<ul>
<li><b>Strictly Necessary.</b> This includes Technologies that allow you access to our Services, applications, and tools that are required to identify irregular website behavior, prevent fraudulent activity and improve security or that allow you to make use of our Services;</li>
<br>
<li><b>Functional.</b> We may use Technologies that allow us to offer you enhanced functionality when accessing or using our Services. This may include identifying you when you sign into our Services or keeping track of your specified preferences, interests, or past items viewed;</li>
<br>
<li><b>Analytics.</b> We may use Technologies to assess the performance of our Services, including as part of our analytic practices to help us understand how individuals use our Services;</li>
<br>
<li><b>Advertising or Targeting.</b> We may use first party or third-party Technologies to deliver content, including ads relevant to your interests, on our Services or on third-party websites.</li>
</ul>

You are encouraged to review each cookie vendor’s privacy policies and contact them directly for responses to your questions. We may also use third-party vendors to authenticate your web traffic: see “<a href="#heading7">What Are My Rights with Respect To My Personal Data?</a>” below for more information on how to opt out of some of this collection.

Note: You should be aware that getting a new computer, installing a new Internet browser, upgrading an existing browser, or erasing or otherwise altering your Internet browser’s cookies may also clear certain opt-out cookies, plugins or settings. 

Permission may use the following “cookies” to track your visit:

<table>
<thead>
<tr>
<th>Provider</th>
<th>Purpose</th>
</tr>
</thead>
<tbody>
<tr>
<td>Auth0</td>
<td>To handle user registration, login, and authentication.</td>
</tr>
<tr>
<td>Google Analytics</td>
<td>To measure effectiveness of advertising and users’ interaction with content.</td>
</tr>
<tr>
<td>Facebook</td>
<td>To enable functionality with social media features.</td>
</tr>
<tr>
<td>Twitter</td>
<td>To enable functionality with social media features.</td>
</tr>
<tr>
<td>YouTube</td>
<td>To enable functionality with social media features.</td>
</tr>
<tr>
<td>LinkedIn</td>
<td>To enable functionality with social media features.</td>
</tr>
<tr>
<td>Medium</td>
<td>To enable functionality with social media features.</td>
</tr>
<tr>
<td>Reddit</td>
<td>To enable functionality with social media features.</td>
</tr>
<tr>
<td>Telegram</td>
<td>To enable functionality with social media features.</td>
</tr>
<tr>
<td>GitLab</td>
<td>To enable functionality and link to our code repository.</td>
</tr>
<tr>
<td>Sendinblue</td>
<td>To orchestrate email communication with Members.</td>
</tr>
<tr>
<td>AddThis</td>
<td>For targeted advertising.</td>
</tr>
<tr>
<td>Google Ad Exchange/DoubleClick</td>
<td>For targeted advertising.</td>
</tr>
</tbody>
</table>

<h3>Tracking pixels</h3>
Our newsletter and emails sent from or on behalf of Permission may contain tracking pixels, or a transparent image embedded in emails to enable log file recording and analysis. We use information collected in this manner to perform statistical analysis of the success or failure of online marketing and customer outreach efforts. Based on the embedded tracking pixel, we may be able to determine if and when an email was opened, and which links in the email were accessed.

Personal data collected using tracking pixels is stored and analyzed by us to optimize the delivery of our newsletters and emails, and to improve the relevance of the distributed content. Many email clients and web browsers support functionality to opt out or prevent the use of these tracking mechanisms. You may also revoke your consent to receiving our newsletter at any time.

<h2><a id="heading3"></a>3.	How long do we keep your data?</h2>
We only process and keep any personal data that you share with us for as long as needed to achieve the purpose of storage, as long as consent is maintained, or as long as is granted by laws or regulations we are subject to. The exact length of time we keep personal data depends on the respective statutory retention period for that type of information. After that period of time passes, or if storage of personal data is not applicable, personal data is routinely blocked, deleted or erased.

<h2><a id="heading4"></a>4.	How do we use your information?</h2>
Having accurate information about you helps us provide a smooth, efficient, and customized experience. Generally speaking, we use any information we collect to provide services to you, keep our Services running smoothly, and protect us legally. More specifically, we may use information collected about you to:

<ul>
 	<li>Create and manage your account</li>
 	<li>Contact you about your account or orders</li>
 	<li>Send you a newsletter once you successfully subscribe</li>
 	<li>Respond to your comments, questions and requests and provide customer service</li>
 	<li>Send you technical notices, updates, security alerts and support and administrative messages</li>
 	<li>Compile aggregated or de-identified statistical data for use internally or with third parties</li>
 	<li>Maintain and improve the efficiency and operation of our Services and products</li>
 	<li>Assist with the development of our products and other purposes related to Permission’s business</li>
 	<li>Monitor and analyze usage and trends to improve your experience with our Services and products</li>
 	<li>Process and deliver contest entries and rewards</li>
 	<li>Assist law enforcement and respond to subpoenas, and to resolve disputes and troubleshoot problems</li>
 	<li>To prevent account fraud and for security purposes</li>
</ul>

In accordance with applicable law, information covered by this Policy may be transferred to, and processed in, the United States or any other country in which Permission or its affiliates, subsidiaries or service providers maintain facilities, even if the level of data privacy required in that country is less than that required by the European Union or other jurisdictions. We endeavor to protect your data in compliance with all applicable laws.  We may engage in automated decision making, including profiling. Our processing of your personal information will not result in a decision based solely on automated processing that significantly affects you unless such a decision is necessary as part of a contract we have with you, we have your consent, or we are permitted by law to engage in such automated decision making. If you have questions about our automated decision making, you may contact us as set forth below.

<h2><a id="heading5"></a>5.	When do we share your information?</h2>
<h3>To obey the law or protect rights</h3>
If we believe the release of information about you is necessary under any applicable law, rule or regulation, to respond to legal process, to investigate or remedy potential violations of our policies or fraudulent activities, or when we believe in good faith that disclosure is necessary to protect our rights, property, and safety, we may share your information, including exchanging information with other entities for fraud protection and credit risk reduction. 

<h3>With Service Providers to support necessary business activities</h3>
We may share your personal with third parties necessary to provide you with services you have requested, such as our hosting, identity verification services (including Know Your Consumer (KYC) verification information where required under applicable law), email service, analytics, customer service, parcel delivery service, event or campaign management providers. These parties are authorized to use your personal data only as necessary to provide these services to us or on our behalf. 

<h3>With our Advertising Partners</h3>
We may also share your personal information with third-party advertising partners. These third-party advertising partners may set Technologies and other tracking tools on our Services to collect information regarding your activities and your device (e.g., your IP address, cookie identifiers, page(s) visited, location, time of day). These advertising partners may use this information (and similar information collected from other services) for purposes of delivering personalized advertisements to you when you visit digital properties within their networks. This practice is commonly referred to as “interest-based advertising” or “personalized advertising.” 

We may also partner with other companies that offer products or services related to ours or that host or sponsor related events. In such instances, we may share your information with these business partners if you provide your personal information to event sponsors at their booths or presentations.

<h3>If you choose to share it with other users</h3>
Our Services offer publicly accessible blogs or community forums. You should be aware that any information you provide in these areas may be read, collected, and used by others who access them. To request removal of your personal information from our blog or community forum, contact us at dpo@permission.io.

We also provide certain social media features on our Services, such as the Facebook Like button and widgets, such as the “Share This” button or other interactive mini-programs that run on our Services. These features may collect your IP address, which page you are visiting on our websites, and may set a cookie to enable the feature to function properly. Social media features and widgets are either hosted by a third party or hosted directly on our Services. Your interactions with these features are governed by the privacy policy of the company providing the feature.

Additionally, for certain features on our websites, specifically those for applying to a job opening at Permission, you may use sign-in services such as LinkedIn or other OpenID providers. Services like LinkedIn may give you the option to post information about your activities on our Services to your profile page to share with others within your network.

Note: Permission does not endorse or make any representations about third-party websites. We encourage you to carefully read the privacy policy of any website you visit.

<h3>In the event of a merger or acquisition</h3>
If Permission is involved in a merger, acquisition, financing due diligence, reorganization, bankruptcy, receivership, purchase or sale of assets, or transition of service to another provider, your information may be shared with the successor entity or third party. Where required by law, you will be notified by email and/or a prominent notice on our Services of any change in ownership or uses of your personal data, as well as any choices you may have regarding your personal data.

<h2><a id="heading6"></a>6.	Third-party Sites</h2>
Our Services may contain links to third-party websites and applications of interest, including advertisements, affiliate links and external services, that may or may not be connected with us. Additionally, some of our Services may use framing techniques to serve content to or from our partners while preserving the look and feel of our Services. Once you have used these links, any information you provide to these third parties is not covered by this Privacy Policy, and we cannot guarantee the safety and privacy of your information. Before visiting and providing any information to any third-party websites, we encourage you to inform yourself of the privacy policies and practices (if any) of the third party responsible for that website. You should take those steps necessary to protect the privacy of your information as you see fit. We are not responsible for the content or privacy and security practices and policies of any third parties, including other websites, services or applications that may be linked to or from the Services.


<h2><a id="heading7"></a>7.	What are My Rights with Respect To My Personal Data?</h2>
We recognize that you have certain rights related to your personal data. We feel that your privacy and ability to preserve and exercise your rights is very important. You are encouraged to review and understand these rights as they pertain to you and your personal data. Subject to applicable law, you may have the following rights:

<ul>
<li>Access Personal Information about you, including: (i) confirming whether we are processing your personal information; (ii) obtaining access to or a copy of your personal information; and (iii) receiving an electronic copy of personal information that you have provided to us, or asking us to send that information to another company (the “right of data portability”);</li>
<li>Request Correction of your personal information where it is inaccurate or incomplete. In some cases, we may provide self-service tools that enable you to update your personal information;</li>
<li>Request Deletion of your personal information;</li>
<li>Request Restriction of or Object to our processing of your personal information; and</li>
<li>Withdraw your Consent to our processing of your personal information.</li>
</ul>

<h3>Exercising your rights</h3>
In support of these rights, upon request and subject to applicable law, Permission will provide you with information about whether we hold any of your personal data if you contact us as provided below in “<a href="#heading14">How Can You Contact Us About This Policy.</a>” You may update, correct or delete information about you at any time by logging into your account and updating your preferences. To opt-out of the sharing of your personal information with LiveRamp, please <a href="https://urldefense.com/v3/__https:/optout.liveramp.com/opt_out__;!!HJT62s_lzg!h-6BCz1L-JPIdrKwsW8hThWfOWaaFjcrehOjtjKYJpDZY6aE9xFXBq8WFSmLFmTj$" target="_blank" rel="noopener">click here</a>.

If you would at any time like to review or change the information in your account or terminate your account, you can do so by logging into your account settings and updating your user account. Upon your request to terminate your account, we will deactivate or delete your account and information from our active databases. However, some information may be retained in our files to prevent fraud, troubleshoot problems, assist with any investigations, comply with legal requirements and/or enforce our Terms of Use. Note: We will retain and use your information, including cached or archived copies, as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. If you have become aware that an account has been created about you without your knowledge or consent, you may contact us at dpo@permission.io to request deletion of that account.

For your protection, we may only implement requests with respect to the personal information associated with the particular email address that you use to send us your request, and we may need to verify your identity before implementing your request, such as by asking you to provide information you used to create your account, or by asking you questions about your use of our Services. We will process all requests in accordance with applicable law.

<h3>Opting out of emails</h3>
To opt out of newsletters and/or email marketing from us, you may indicate your preference by following the unsubscribe instructions provided in the newsletter, or by changing your preferences within your account settings. Please note that we may still send you notices of any updates to our Terms of Use or Privacy Policy where permitted by law.

<h3>Do-Not-Track signals</h3>
Many web browsers and some mobile operating systems include a Do-Not-Track (“DNT”) feature or setting you can activate to signal your privacy preference not to have data about your online browsing activities monitored and collected. We do not currently respond to DNT browser signals.

<h2><a id="heading8"></a>8.	How does Permission protect my information?</h2>
Permission takes reasonable administrative, technical and physical security measures to help protect your personal data from loss, theft, misuse and unauthorized access, disclosure, alteration and destruction. We follow generally accepted standards to protect the personal data submitted to us, both during transmission and once it is received, taking into account the nature of such data and the risks involved in processing, and comply with applicable laws and regulations.

While we have taken reasonable steps to secure the personal data you provide to us, please be aware that no security measures are perfect or impenetrable, and no method of data transmission can be guaranteed against any interception or other type of misuse. Any information disclosed online is vulnerable to interception and misuse by unauthorized parties. Therefore, we cannot guarantee complete security if you provide personal data via our Services.

If you have any questions about security or any reason to believe that your interaction with us is no longer secure (for example, if you feel that the security of your account has been compromised), please contact us at security@permission.io.

<h2><a id="heading9"></a>9.	Policy for Minors</h2>
We do not knowingly solicit information from or market to children under the age of thirteen (13). By using the Services, you represent that you are at least 13 or that you are the parent or guardian of such a minor and consent to such minor dependent’s use of the Services. If we learn that personal information from users less than 13 years of age has been collected, we will deactivate the account and take reasonable measures to promptly delete such data from our records, except where we are required to retain it under applicable law. If you are a parent or guardian and believe we have collected from your child under age 13, please contact us with the information below.

Our Services and products are not intended for, nor designed to attract individuals under the age of thirteen (13). Permission does not knowingly collect personally identifiable information from any person under the age of thirteen (13).

<h2><a id="heading10"></a>10.	California Privacy Rights</h2>
This Supplemental California Privacy Notice only applies to our processing of personal information that is subject to the California Consumer Privacy Act of 2018 (“CCPA”). The CCPA provides California residents with the right to know what categories of personal information Permission has collected about them and whether Permission disclosed that personal information for a business purpose (e.g., to a service provider) in the preceding 12 months. California residents can find this information below:

<table>
<thead>
<tr>
<th>Category of Personal Information Collected by Permission</th>
<th>Category of Third Parties Information is Disclosed to for a Business Purpose</th>
</tr>
</thead>
<tbody>
<tr>
<td><b>Identifiers.</b> 
A real name, alias, postal address, unique personal identifier, online identifier, Internet Protocol address, email address, account name, Social Security number, or other similar identifiers. 
</td>
<td>•	Advertising networks
•	Data analytics providers
•	Consumer data resellers (not including SSN)
•	Service providers
</td>
</tr>
<tr>
<td><b>Personal information categories listed in the California Customer Records statute (Cal. Civ. Code § 1798.80(e))</b>
A name, signature, Social Security number, physical characteristics or description, address, telephone number, driver's license or state identification card number,  employment, employment history,  credit card number, debit card number, or any other financial information.</td>
<td>•	Advertising networks
•	Data analytics providers
•	Consumer data resellers (not including SSN)
•	Service providers
</td>
</tr>
<tr>
<td><b>Protected classification characteristics under California or federal law</b>
Age, national origin, marital status, sex (including gender).</td>
<td>•	Advertising networks
•	Data analytics providers
•	Consumer data resellers
•	Service providers
</td>
</tr>
<tr>
<td><b>Commercial information</b>
Records of personal property, products or services purchased, obtained, or considered, or other purchasing or consuming histories or tendencies.</td>
<td>•	Advertising networks
•	Data analytics providers
•	Consumer data resellers
•	Service providers</td>
</tr>
<tr>
<td><b>Biometric information</b>
Physiological or biological characteristics that can be used to establish individual identity, such as the user’s face, from which an identifier template such as a faceprint, can be extracted.</td>
<td>Not shared</td>
</tr>
<tr>
<td><b>Internet or other electronic network activity</b>
Browsing history, search history, information on a consumer's interaction with an internet website, application, or advertisement.
</td>
<td>•	Advertising networks
•	Data analytics providers
•	Consumer data resellers
•	Service providers
</td>
</tr>
<tr>
<td><b>Inferences drawn from other personal information to create a profile about a consumer</b>
Profile reflecting a consumer's preferences, characteristics, psychological trends, predispositions, behavior, attitudes, intelligence, abilities, and aptitudes.
</td>
<td>•	Advertising networks
•	Data analytics providers
•	Consumer data resellers
•	Service providers
</td>
</tr>
</tbody>
</table>

The categories of sources from which we collect personal information and our business and commercial purposes for using personal information are set forth in “Personal Data We Collect” and “How do we use your Information?” above, respectively. 

Although we may allow other third-party advertising partners to use cookies and similar Technologies to collect your information for interest-based advertising purposes (as described above in “When do we share your information?”), Permission only shares the personal information you provide to us for valuable consideration when you affirmatively authorize us to do so through the Services, in order to obtain ASK digital currency. Therefore, Permission does not “sell” personal information for purposes of the CCPA, nor do we have actual knowledge of any “sale” of personal  information of minors under 16 years of age.

California residents have the right not to receive discriminatory treatment by us for the exercise of their rights conferred by applicable law. Only you, or someone legally authorized to act on your behalf, may make a verifiable consumer request related to your personal information. You may also make a verifiable consumer request on behalf of your minor child. To designate an authorized agent, please contact us as set forth in “How Can You Contact Us About This Policy” below and provide written authorization signed by you and your designated agent. To protect your privacy, we will take steps the following steps to verify your identity before fulfilling your request. When you make a request, we will ask you to provide sufficient information that allows us to reasonably verify you are the person about whom we collected personal information or an authorized representative, which may include asking you to answer questions regarding your account and use of our Services.

This Privacy Policy uses industry-standard technologies and was developed in line with the World Wide Web Consortium’s Web Content Accessibility Guidelines, version 2.1. If you wish to print this policy, please do so from your web browser or by saving the page as a PDF.

California Civil Code Section 1798.83, also known as the “Shine The Light” law, permits our users who are California residents to request and obtain from us, once a year and free of charge, information about categories of personal information (if any) we disclosed to third parties for direct marketing purposes and the names and addresses of all third parties with which we shared personal information in the immediately preceding calendar year. If you are a California resident and would like to make such a request, please submit your request in writing to us using the contact information provided below.

<h2><a id="heading11"></a>11. Supplemental Notice for Nevada Residents</h2>
If you are a resident of Nevada, you have the right to opt-out of the sale of certain Personal Information to third parties who intend to license or sell that Personal Information. You can exercise this right by contacting us at dpo@permission.io with the subject line “Nevada Do Not Sell Request” and providing us with your name and the email address associated with your account. Please note that we do not currently sell your Personal Information as sales are defined in Nevada Revised Statutes Chapter 603A – to the extent that receive monetary consideration for disclosing your data, it is only in the context in which you provide the information to operator for the purpose of receiving ASK digital currency. If you have any questions, please contact us as set forth below.

<h2><a id="heading12"></a>12.	Notice to All Non-US Residents</h2>
Our servers are located in the US. If you are located outside of the US, please be aware that any information provided to us, including personal information, may be transferred from your country of origin to the US. We endeavor to protect all personal information consistent with the requirements of applicable laws. 

<h2><a id="heading13"></a>13.	Changes to This Policy</h2>
If we make material changes, we will notify you by revising the date at the top of the Policy, and where required by law, we may provide you with more prominent notice (such as adding a statement to our homepage or sending you an email notification).

We encourage you to review the Policy whenever you access the Services to stay informed about our information practices and the ways you can help protect your privacy. 

<h2><a id="heading14"></a>14.	How Can You Contact Us About This Policy</h2>
If you have questions or comments about this policy, you may contact our Data Protection Officer (DPO) at dpo@permission.io or via mail at 888 Prospect Street, Suite 200, La Jolla CA 92037. 


